package org.super8.leto.ui.view

import android.content.Context
import android.content.Intent
import android.util.AttributeSet
import android.util.Log
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import android.widget.FrameLayout
import android.widget.ImageView
import kotlinx.android.synthetic.main.view_search.view.*
import org.super8.leto.R


class SearchView : FrameLayout {

    var searchActionListener: OnSearchActionListener? = null

    constructor(context: Context) : super(context) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init()
    }

    private fun init() {
        View.inflate(context, R.layout.view_search, this)

        findViewById<EditText>(R.id.toEditText)?.setOnEditorActionListener({ v, actionId, event ->
            Log.d(javaClass.simpleName, "init: key ${event?.keyCode}")
            if (event?.keyCode == KeyEvent.KEYCODE_ENTER || actionId == EditorInfo.IME_ACTION_DONE) {
                searchActionListener?.onSearch(fromEditText.text.toString(), toEditText.text.toString())
            }
            false
        })

        findViewById<ImageView>(R.id.changeImageView).setOnClickListener {
            try {
                val i = Intent()
                i.action = "com.company.plugin.do"
                i.type = "text/plain"
                i.putExtra("KEY", "This is the text message sent from Android")
                context.startActivity(i)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }


    interface OnSearchActionListener {
        fun onSearch(from: String, to: String)
    }
}