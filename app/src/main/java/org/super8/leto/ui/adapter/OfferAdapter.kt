package org.super8.leto.ui.adapter

import android.annotation.SuppressLint
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.item_offer.view.*
import org.super8.leto.R
import org.super8.leto.model.OfferModel
import org.super8.leto.model.Trip
import org.super8.leto.util.SharedPreference
import org.super8.leto.util.loadImage

class OfferAdapter(
        val items: ArrayList<OfferModel>,
        private val userTripsList: List<Trip?>,
        private val onOfferClickListener: OnOfferClickListener)
    : RecyclerView.Adapter<OfferAdapter.ViewHolder>() {


    private val userProgress by lazy { SharedPreference.userProgress }

    override fun getItemCount() = items.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int)
            = ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_offer, parent, false))

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = items[position]

        with(holder.view) {
            val savedProgress = userProgress?.keys?.toList()?.getOrNull(userProgress?.values?.indexOfFirst { item.companyModel == it } ?: -1) ?: 0
            val userBonusMiles = item.companyModel.calculateUserBonusMiles(userTripsList) + savedProgress
            val canBuyOffer = userBonusMiles >= item.requiredMiles

            if (canBuyOffer) {
                actionButton.text = "Получить"
            } else {
                actionButton.text = "Докупить"
            }

            imageView.loadImage(item.companyModel.img)
            progressLayout.maxProgress = item.requiredMiles.toFloat()
            progressLayout.currentProgress = userBonusMiles.toFloat()
            destinationLabel.text = item.location.origin.name + " - " + item.location.destination.name
            progressLabel.text = "${userBonusMiles.toInt()} / ${item.requiredMiles}"

            actionButton.setOnClickListener {
                if (canBuyOffer) {
                    onOfferClickListener.onBuyOfferClick(item)
                } else {
                    onOfferClickListener.onBuyPointsClick(item)
                }
            }
        }
    }

    class ViewHolder(val view: View) : RecyclerView.ViewHolder(view)

    interface OnOfferClickListener {

        fun onBuyOfferClick(offerModel: OfferModel)

        fun onBuyPointsClick(offerModel: OfferModel)
    }
}