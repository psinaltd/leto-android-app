package org.super8.leto.ui.dialog

import android.content.Context
import android.support.v7.app.AlertDialog
import org.super8.leto.R

class BuyDialog(context: Context, destination: String, onBuyClickListener: () -> Unit) {

    init {
        AlertDialog.Builder(context)
                .setIcon(R.drawable.credit_card)
                .setTitle("Купить билет в $destination")
                .setMessage("У вас достаточно бонусных миль, чтобы приобрести билет. Открыть окно покупки?")
                .setPositiveButton("Открыть", { _, _ -> onBuyClickListener.invoke() })
                .setNegativeButton("Отмена", null)
                .create()
                .show()
    }
}