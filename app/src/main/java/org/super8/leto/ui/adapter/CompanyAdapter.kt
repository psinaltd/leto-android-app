package org.super8.leto.ui.adapter

import android.annotation.SuppressLint
import android.content.res.ColorStateList
import android.graphics.Color
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.firebase.firestore.Query
import kotlinx.android.synthetic.main.item_company_card.view.*
import org.super8.leto.R
import org.super8.leto.model.CompanyModel
import org.super8.leto.model.OfferModel
import org.super8.leto.model.TravelLocation
import org.super8.leto.model.Trip
import org.super8.leto.util.SharedPreference
import org.super8.leto.util.loadImage

class CompanyAdapter(
        query: Query,
        private val location: TravelLocation,
        private val distanceMiles: Int,
        private val userTripsList: List<Trip?>,
        private val onCompanyClickListener: OnOfferClickListener)
    : FirestoreAdapter<CompanyAdapter.ViewHolder>(query) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int)
            = ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_company_card, parent, false))

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = getSnapshot(position).toObject(CompanyModel::class.java)

        with(holder.view) {
            Log.d(javaClass.simpleName, "onBindViewHolder: item = $item")
            val saleOffer = item.sales?.find {
                Log.d(javaClass.simpleName, "onBindViewHolder: it.from = ${it.from} + ${location.origin.formattedAddress} _ ${it.to} + ${location.destination.formattedAddress}")
                it.from == location.origin.formattedAddress && it.to == location.destination.formattedAddress
            }


            if (saleOffer != null) {
                val colorStateList = ColorStateList.valueOf(Color.parseColor("#97921b"))
                progressLabel.setTextColor(colorStateList)
                progressBar.progressTintList = colorStateList
            } else {
                val colorStateList = ColorStateList.valueOf(Color.parseColor("#29B6F6"))
                progressLabel.setTextColor(colorStateList)
                progressBar.progressTintList = colorStateList
            }

            val userProgress = SharedPreference.userProgress

            val savedProgress = userProgress?.keys?.toList()?.getOrNull(userProgress.values.indexOfFirst { item == it }) ?: 0
            val userBonusMiles = item.calculateUserBonusMiles(userTripsList).toInt() + savedProgress
            val requiredMiles = item.calculateDistanceBonusMiles(distanceMiles, saleOffer?.coef)
            imageView.loadImage(item.img)
            progressBar.max = requiredMiles
            progressBar.progress = userBonusMiles
            progressLabel.text = "$userBonusMiles/$requiredMiles M"

            rootView.setOnClickListener {
                onCompanyClickListener.onOfferClick(OfferModel(item, location, requiredMiles))
            }
        }
    }


    class ViewHolder(val view: View) : RecyclerView.ViewHolder(view)

    interface OnOfferClickListener {

        fun onOfferClick(offerModel: OfferModel)
    }
}