package org.super8.leto.ui.activity

import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.util.DisplayMetrics
import android.util.Log
import android.view.View
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.firebase.firestore.FirebaseFirestore
import com.sothree.slidinguppanel.SlidingUpPanelLayout
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.bottom_panel.*
import kotlinx.android.synthetic.main.item_offer.view.*
import org.super8.leto.App
import org.super8.leto.R
import org.super8.leto.model.*
import org.super8.leto.network.AitaApi
import org.super8.leto.network.MapsApi
import org.super8.leto.network.simpleNetworkRequest
import org.super8.leto.ui.adapter.CompanyAdapter
import org.super8.leto.ui.adapter.OfferAdapter
import org.super8.leto.ui.dialog.AddOfferDialog
import org.super8.leto.ui.dialog.BuyDialog
import org.super8.leto.ui.dialog.ConverterDialog
import org.super8.leto.ui.dialog.ProgressBarDialog
import org.super8.leto.ui.view.SearchView
import org.super8.leto.util.*
import rx.Observable
import javax.inject.Inject


class MainActivity : AppCompatActivity(), OnMapReadyCallback, CompanyAdapter.OnOfferClickListener {

    @Inject lateinit var aitaApi: AitaApi
    @Inject lateinit var mapsApi: MapsApi

    private var mMap: GoogleMap? = null
    private var tempPath: TempPatch? = null
    private var userModel: UserModel? = null
    private var progressDialog: ProgressBarDialog? = null
    private var firebaseFirestore: FirebaseFirestore? = null
    private var userTripsList: List<Trip?>? = null
    private var companyAdapter: CompanyAdapter? = null

    private fun showProgressDialog() {
        progressDialog = ProgressBarDialog(this)
    }

    private fun hideProgressDialog() {
        progressDialog?.dismiss()
        progressDialog = null
    }

    override fun onMapReady(googleMap: GoogleMap?) {
        mMap = googleMap

        drawUserOffers(googleMap)


        simpleNetworkRequest(aitaApi.getUserTrips(), {
            if (it.error == null) {
                userTripsList = it.data?.trips
                updateOffersList()


                var isCameraPosed = false

                it.data?.trips?.forEach { trip ->
                    val patches = trip?.flights?.map { it?.origin to it?.destination }

                    patches?.map {
                        Observable.zip(mapsApi.getLocationByAddress(it.first?.name!!),
                                mapsApi.getLocationByAddress(it.second?.name!!), ::PathZipModel)
                    }?.forEach {
                        simpleNetworkRequest(it, {
                            val originLocation = it.origin.results?.firstOrNull()
                            val destinationLocation = it.destination.results?.firstOrNull()
                            if (originLocation != null && destinationLocation != null) {
                                val patchColor = Color.parseColor("#3E95BB")
                                googleMap?.markerAt(originLocation.getLocation(), applicationContext)
                                googleMap?.drawPath(listOf(originLocation.getLocation(), destinationLocation.getLocation()), patchColor)
                                googleMap?.markerAt(destinationLocation.getLocation(), applicationContext)

                                if (!isCameraPosed) {
                                    isCameraPosed = true
                                    mMap?.moveCamera(CameraUpdateFactory.newLatLng(originLocation.getLocation()))
                                }
                            } else {
                                Log.d(javaClass.simpleName, "onMapReady: can't fetch location of $it")
                            }
                        }, {
                            Log.d(javaClass.simpleName, "onMapReady: oops")
                        })
                    }
                }
            } else {
                Log.d(javaClass.simpleName, "onMapReady: error = ${it.error}")
            }
        }, {
            Log.d(javaClass.simpleName, "onCreate: error ${getString(it)}")
        })
    }

    private fun drawUserOffers(googleMap: GoogleMap?) {
        val userOffers = SharedPreference.userOffers
        if (userOffers?.isNotEmpty() == true) {
            userOffers.forEach {
                val patchColor = Color.parseColor("#bdbdbd")
                val originLoc = it.location.origin.getLocation()
                val destLoc = it.location.destination.getLocation()
                googleMap?.markerAt(originLoc, applicationContext, R.drawable.oval_gray)
                googleMap?.drawPath(listOf(originLoc, destLoc), patchColor)
                googleMap?.markerAt(destLoc, applicationContext, R.drawable.oval_gray)
            }
        }
    }

    override fun onOfferClick(offerModel: OfferModel) {
        val canBuyOffer = offerModel.companyModel.calculateUserBonusMiles(userTripsList!!) > offerModel.requiredMiles
        if (canBuyOffer) {
            BuyDialog(this, offerModel.location.destination.name!!) {
                val url = Uri.parse("https://www.google.ru/search?q=${offerModel.companyModel.name} " +
                        "купить билет из ${offerModel.location.origin.name} в ${offerModel.location.destination.name}")
                val i = Intent(Intent.ACTION_VIEW, url)
                startActivity(i)
            }
        } else {
            AddOfferDialog(this) {
                SharedPreference.userOffers = SharedPreference.userOffers?.apply { add(offerModel) }
                slidingPanelLayout.panelState = SlidingUpPanelLayout.PanelState.EXPANDED
                updateOffersList()
                drawUserOffers(mMap!!)
                mMap?.moveCamera(CameraUpdateFactory.newLatLng(offerModel.location.origin.getLocation()))
            }
        }
    }

    private fun updateOffersList() {
        if (userTripsList != null) {
            offerRecyclerView.adapter = OfferAdapter(SharedPreference.userOffers!!, userTripsList!!, object : OfferAdapter.OnOfferClickListener {
                override fun onBuyOfferClick(offerModel: OfferModel) {
                    SharedPreference.userProgress = SharedPreference.userProgress?.apply {
                        val currentValue = keys.toList().getOrNull(values.indexOfFirst { offerModel.companyModel == it })
                        if (currentValue != null) {
                            remove(currentValue)
                            put(currentValue - offerModel.requiredMiles, offerModel.companyModel)
                        }
                    }
                    updateOffersList()
                    companyAdapter?.notifyDataSetChanged()


                    AlertDialog.Builder(this@MainActivity)
                            .setView(R.layout.dialog_happy)
                            .create()
                            .show()
                }

                override fun onBuyPointsClick(offerModel: OfferModel) {
                    ConverterDialog(this@MainActivity, offerModel, userTripsList!!) { distance ->
                        SharedPreference.userProgress = SharedPreference.userProgress?.apply {
                            val currentValue = keys.toList().getOrNull(values.indexOfFirst { offerModel.companyModel == it })
                            if (currentValue != null) {
                                remove(currentValue)
                                put(currentValue + distance!!, offerModel.companyModel)
                            } else {
                                put(distance!!, offerModel.companyModel)
                            }
                        }
                        updateOffersList()
                        companyAdapter?.notifyDataSetChanged()
                        Log.d(javaClass.simpleName, "onBuyPointsClick: update ")
                    }
                }
            })
        } else {
            Log.d(javaClass.simpleName, "updateOffersList: userTripsList == null")
        }
    }

    private fun updateBottomPanelState() {
        async({ SharedPreference.userOffers }) {
            if (it?.isNotEmpty() == true) {
                slidingPanelLayout.panelState = SlidingUpPanelLayout.PanelState.COLLAPSED
            } else {
                slidingPanelLayout.panelState = SlidingUpPanelLayout.PanelState.HIDDEN
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        App.appComponent.inject(this)
        FirebaseFirestore.setLoggingEnabled(true)
        firebaseFirestore = FirebaseFirestore.getInstance()

        val mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        updateBottomPanelState()
        makeCoolBackground()

        searchView.searchActionListener = object : SearchView.OnSearchActionListener {
            override fun onSearch(from: String, to: String) {
                showProgressDialog()

                val locationRequest = Observable.zip(mapsApi.getLocationByAddress(from),
                        mapsApi.getLocationByAddress(to), ::PathZipModel)

                simpleNetworkRequest(locationRequest, {
                    hideProgressDialog()
                    val originLocation = it.origin.results?.firstOrNull()
                    val destinationLocation = it.destination.results?.firstOrNull()
                    if (originLocation != null && destinationLocation != null) {
                        val patchColor = Color.parseColor("#FFA726")
                        if (tempPath != null) {
                            tempPath?.clean()
                        }

                        if (mMap != null) {
                            mMap!!.moveCamera(CameraUpdateFactory.newLatLng(originLocation.getLocation()))

                            val originLatLng = originLocation.getLocation()
                            val destinationLatLng = destinationLocation.getLocation()
                            tempPath = TempPatch(
                                    mMap!!.drawPath(listOf(originLatLng, destinationLatLng), patchColor),
                                    mMap!!.markerAt(originLatLng, applicationContext, R.drawable.oval_orange),
                                    mMap!!.markerAt(destinationLatLng, applicationContext, R.drawable.oval_orange))

                            val query = firebaseFirestore?.collection("company")
                                    ?.limit(10000)

                            if (query != null) {
                                val distance = mMap?.distance(originLatLng, destinationLatLng)!! / 1000 * 0.621371

                                val location = TravelLocation(originLocation, destinationLocation)

                                companyAdapter = CompanyAdapter(query, location, distance.toInt(), userTripsList!!, this@MainActivity)
                                companyRecyclerView.adapter = companyAdapter
                                companyAdapter?.setQuery(query)
                            } else {
                                Snackbar.make(slidingPanelLayout, "Что-то пошло не так. Не удалось загрузить акции.", Snackbar.LENGTH_LONG)
                            }
                        } else {
                            Snackbar.make(slidingPanelLayout, "Что-то пошло не так. Не удалось загрузить карту.", Snackbar.LENGTH_LONG)
                        }
                    } else {
                        Snackbar.make(slidingPanelLayout, "Ошибка во время обработки", Snackbar.LENGTH_LONG)
                    }
                }, {
                    hideProgressDialog()
                    Snackbar.make(slidingPanelLayout, "Ошибка во время загрузки", Snackbar.LENGTH_LONG)
                })
            }
        }
    }

    private fun makeCoolBackground() {
        val display = windowManager.defaultDisplay
        val outMetrics = DisplayMetrics()
        display.getMetrics(outMetrics)

        val density = resources.displayMetrics.density
        val dpHeight = outMetrics.heightPixels / density
        val dpWidth = outMetrics.widthPixels / density

        slidingPanelLayout.addPanelSlideListener(object : SlidingUpPanelLayout.PanelSlideListener {
            override fun onPanelSlide(panel: View?, slideOffset: Float) {
                val fl = slideOffset * dpWidth
                background.translationY = fl
                planeIcon.rotation = map(slideOffset, 0f, 1f, 0f, 180f)
                offersContainer.translationY = map(slideOffset, 0f, 1f, offersContainer.height.toFloat(), 0f)
                var i = 0
                while (i < offerRecyclerView.childCount) {
                    val holder = offerRecyclerView.getChildViewHolder(offerRecyclerView.getChildAt(i))
                    val progressLayout = holder.itemView.progressLayout
                    progressLayout.currentProgress = map(slideOffset, 0f, 1f, progressLayout.maxProgress, progressLayout.firstValue ?: 0f)
                    ++i
                }
            }

            override fun onPanelStateChanged(panel: View?, previousState: SlidingUpPanelLayout.PanelState?, newState: SlidingUpPanelLayout.PanelState?) {

            }

        })
    }
}
