package org.super8.leto.ui.dialog

import android.app.Dialog
import android.content.Context
import org.super8.leto.R

class ProgressBarDialog(context : Context) {
    var dialog: Dialog? = null

    init {
        dialog = Dialog(context, R.style.ProgressDialog)
        dialog!!.setContentView(R.layout.dialog_progress)
        dialog!!.setCancelable(false)
        dialog!!.show()
    }

    fun dismiss(){
        dialog?.dismiss()
    }
}