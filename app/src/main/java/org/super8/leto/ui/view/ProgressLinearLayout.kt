package org.super8.leto.ui.view

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.util.AttributeSet
import android.widget.LinearLayout
import org.super8.leto.util.map

class ProgressLinearLayout : LinearLayout {

    var maxProgress = 0f
    var currentProgress = 0f
        set(value) {
            field = value
            if (firstValue == null)
                firstValue = value

            invalidate()
        }

    var firstValue: Float? = null

    private val progressPaint = Paint().apply {
        color = Color.parseColor("#42A5F5")
    }

    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    override fun dispatchDraw(canvas: Canvas?) {
        if (canvas != null) {

            val currentProgressPixel = canvas.width / 100f * map(currentProgress, 0f, maxProgress, 0f, 100f)
            canvas.drawRect(0f, 0f, currentProgressPixel, canvas.height.toFloat(), progressPaint)
        }
        super.dispatchDraw(canvas)
    }
}