package org.super8.leto.ui.dialog

import android.content.Context
import android.support.v7.app.AlertDialog
import android.widget.SeekBar
import android.widget.TextView
import org.super8.leto.R
import org.super8.leto.model.OfferModel
import org.super8.leto.model.Trip
import org.super8.leto.util.SharedPreference.userProgress

class ConverterDialog(context: Context,
                      offerModel: OfferModel,
                      userTripsList: List<Trip?>,
                      onConvertComplete: (Int?) -> Unit) {

    init {
        AlertDialog.Builder(context)
                .setView(R.layout.dialog_converter)
                .setPositiveButton("Обменять", null)
                .setNegativeButton("Отмена", null)
                .create()
                .apply {
                    val savedProgress = userProgress?.keys?.toList()?.getOrNull(userProgress?.values?.indexOfFirst { offerModel.companyModel == it } ?: -1) ?: 0

                    val userBonusMiles = offerModel.companyModel.calculateUserBonusMiles(userTripsList) + savedProgress
                    val requiredMiles = offerModel.requiredMiles
                    val availableMiles = requiredMiles - userBonusMiles.toInt()

                    setOnShowListener {
                        val seekBar = findViewById<SeekBar>(R.id.seekBar)
                        val coastLabel = findViewById<TextView>(R.id.coastLabel)
                        val finishDistanceLabel = findViewById<TextView>(R.id.finishDistanceLabel)

                        setButton(AlertDialog.BUTTON_POSITIVE, "Обменять", { _, _ ->
                            onConvertComplete.invoke(seekBar?.progress)
                        })

                        seekBar?.max = availableMiles
                        coastLabel?.text = "0\$\nвы заплатите"
                        finishDistanceLabel?.text = "$availableMiles\nмиль осталось"


                        seekBar?.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
                            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                                val coast = (progress * 0.05f).toInt()
                                coastLabel?.text = "$coast\$\nвы заплатите"
                                finishDistanceLabel?.text = "${availableMiles - progress}\nмиль осталось"
                            }

                            override fun onStartTrackingTouch(seekBar: SeekBar?) = Unit
                            override fun onStopTrackingTouch(seekBar: SeekBar?) = Unit
                        })
                    }
                }
                .show()
    }
}