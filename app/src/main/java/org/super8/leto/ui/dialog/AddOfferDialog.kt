package org.super8.leto.ui.dialog

import android.content.Context
import android.support.v7.app.AlertDialog
import org.super8.leto.R

class AddOfferDialog(context: Context, onAddOfferClickListener: () -> Unit) {

    init {
        AlertDialog.Builder(context)
                .setIcon(R.drawable.playlist_check)
                .setTitle("Добавить цель")
                .setMessage("Вы хотите добавить это предложение в список ваших целей?")
                .setPositiveButton("Добавить", { _, _ -> onAddOfferClickListener.invoke() })
                .setNegativeButton("Отмена", null)
                .create()
                .show()
    }
}