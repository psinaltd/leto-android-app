package org.super8.leto.model

import android.util.Log

class CompanyModel {

    var img: String? = null
    var coef: Float? = null
    var name: String? = null
    var sales: ArrayList<SaleModel>? = null

    fun calculateDistanceBonusMiles(distanceMiles: Int, coefficient: Float?)
            = (distanceMiles * (coefficient ?: coef!!) * 100).toInt()

    fun calculateUserBonusMiles(userTripsList: List<Trip?>): Double {
        return userTripsList.flatMap { it?.flights!! }
                .filter { it?.carrier?.name == name }
                .sumByDouble { it?.distanceKm ?: 0.0 } * 0.621371
    }


    override fun equals(other: Any?): Boolean {
        if (other?.javaClass == CompanyModel::class.java) {
            val model = other as CompanyModel
            Log.d(javaClass.simpleName, "equals: compare $this to $model")
            Log.d(javaClass.simpleName, "equals: ${model.name == name} ${model.coef == coef} ${model.img == img} ${model.sales == sales}")
            return model.name == name &&
                    model.coef == coef &&
                    model.img == img
        }
        return false
    }

    override fun toString(): String = "CompanyModel( img=$img, coef=$coef, name=$name)"
}

class SaleModel {

    var to: String? = null
    var from: String? = null
    var coef: Float? = null

}