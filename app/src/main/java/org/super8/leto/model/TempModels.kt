package org.super8.leto.model

import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.Polyline


data class PathZipModel(var origin: MapsResponse, var destination: MapsResponse)

data class TempPatch(private var pathLine: Polyline, private var startPoint: Marker, private var endPoint: Marker) {

    fun clean() {
        pathLine.isVisible = false
        startPoint.isVisible = false
        endPoint.isVisible = false
    }
}

data class TravelLocation(val origin: LocationHolder, val destination: LocationHolder)

data class OfferModel(val companyModel: CompanyModel, val location: TravelLocation, var requiredMiles: Int)