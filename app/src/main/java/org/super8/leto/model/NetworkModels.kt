package org.super8.leto.model

import com.google.android.gms.maps.model.LatLng
import com.google.gson.annotations.SerializedName


data class BaseResponse<T>(var status: Int?, var error: String?, var data: T?)

data class UserModel(
        @SerializedName("name") var name: String?, //Aita Traveler
        @SerializedName("id") var id: String?, //4751414573924352
        @SerializedName("kilometers") var kilometers: Int?, //334154
        @SerializedName("email") var email: String? //test@appintheair.mobi
)

data class Trips(
        @SerializedName("more") var more: Boolean?, //true
        @SerializedName("next_url") var nextUrl: String?, //https://iappintheair.appspot.com/api/v1/me/trips?param1=value1&param2=value2&
        @SerializedName("trips") var trips: List<Trip?>?
)

data class Trip(
        @SerializedName("id") var id: String?, //123
        @SerializedName("flights") var flights: List<Flight?>?,
        @SerializedName("hotels") var hotels: List<Hotel?>?,
        @SerializedName("car_rentals") var carRentals: List<CarRental?>?
)

data class Hotel(
        @SerializedName("name") var name: String?, //Test Hotel
        @SerializedName("stars") var stars: Int?, //3
        @SerializedName("address") var address: String?, //John Doe st.
        @SerializedName("checkin_date") var checkinDate: String?, //2018-01-03
        @SerializedName("checkout_date") var checkoutDate: String? //2018-01-05
)

data class CarRental(
        @SerializedName("pickup_date") var pickupDate: String?, //2018-01-03
        @SerializedName("pickup_address") var pickupAddress: String?, //HAN Airport
        @SerializedName("dropoff_date") var dropoffDate: String?, //2018-01-05
        @SerializedName("dropoff_address") var dropoffAddress: String?, //HAN Airport
        @SerializedName("car_name") var carName: String?, //Ford Focus or similar
        @SerializedName("provider") var provider: String? //Vietnam Cars Inc.
)

data class Flight(
        @SerializedName("origin") var origin: Origin?,
        @SerializedName("departure_local") var departureLocal: Int?, //1514973000
        @SerializedName("arrival_local") var arrivalLocal: Int?, //1514980500
        @SerializedName("destination") var destination: Destination?,
        @SerializedName("number") var number: String?, //1232
        @SerializedName("carrier") var carrier: Carrier?,
        @SerializedName("distance_km") var distanceKm: Double?, //1237.7849338823523
        @SerializedName("arrival_utc") var arrivalUtc: Int?, //1514955300
        @SerializedName("departure_utc") var departureUtc: Int? //1514947800
)

data class Carrier(
        @SerializedName("icao") var icao: String?, //HVN
        @SerializedName("iata") var iata: String?, //VN
        @SerializedName("name") var name: String? //Vietnam Airlines
)

data class Origin(
        @SerializedName("country") var country: String?, //VN
        @SerializedName("country_full") var countryFull: String?, //Viet Nam
        @SerializedName("code") var code: String?, //PQC
        @SerializedName("name") var name: String? //Phu Quoc International Airport
)

data class Destination(
        @SerializedName("country") var country: String?, //VN
        @SerializedName("country_full") var countryFull: String?, //Vietnam
        @SerializedName("code") var code: String?, //HAN
        @SerializedName("name") var name: String? //Noi Bai International Airport
)


data class MapsResponse(
        @SerializedName("html_attributions") var htmlAttributions: List<Any?>?,
        @SerializedName("results") var results: List<LocationHolder?>?,
        @SerializedName("status") var status: String? //OK
)

data class LocationHolder(
        @SerializedName("formatted_address") var formattedAddress: String?, //Phú Minh, Sóc Sơn, Hà Nội, Вьетнам
        @SerializedName("geometry") var geometry: Geometry?,
//		@SerializedName("icon") var icon: String?, //https://maps.gstatic.com/mapfiles/place_api/icons/airport-71.png
//		@SerializedName("id") var id: String?, //eda2e01466af32bdb66abe203bbfe32d68233185
        @SerializedName("name") var name: String? //Ханой
//		@SerializedName("photos") var photos: List<Photo?>?,
//		@SerializedName("place_id") var placeId: String?, //ChIJU8SvORUCNTERtCYCUqP64OY
//		@SerializedName("rating") var rating: Float?, //4
//		@SerializedName("reference") var reference: String?, //CmRSAAAA3SwBbbOL6jmoDd3heKQgYuL7LbOXsw9qKV40du2dHct6EJ3WEzhepWF04i05-L7kAOxm4wTWVQ46HCpsOUc7jxsw00HQWYMrNRFulOZRa0LERKK7WtbedjnDjIpiDLkHEhDeKUkxrGLOSzlUeArjfD1FGhSKHwAvfMJQF35LeLvThqz5GHHN_A
//		@SerializedName("types") var types: List<String?>?
) {

    fun getLocation(): LatLng = LatLng(geometry?.location?.lat!!, geometry?.location?.lng!!)
}

data class Geometry(
        @SerializedName("location") var location: Location?
//		@SerializedName("viewport") var viewport: Viewport?
)

data class Location(
        @SerializedName("lat") var lat: Double?, //21.2187149
        @SerializedName("lng") var lng: Double? //105.8041709
)
