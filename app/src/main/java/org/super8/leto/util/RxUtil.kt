package org.super8.leto.util

import rx.Observable
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers


fun <T, R> async(any: () -> T, onNext: (T?) -> R?) =
        Observable.fromCallable { any.invoke() }
                .subscribeOn(Schedulers.io())
                .unsubscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { onNext.invoke(it) }
