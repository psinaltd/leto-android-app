package org.super8.leto.util

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.support.v4.content.ContextCompat
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.*
import org.super8.leto.R
import java.util.*




fun GoogleMap.drawPath(points: List<LatLng>, color: Int): Polyline {

    val path = ArrayList<LatLng>()
    points.forEach { path.add(it) }
    val line = PolylineOptions()
    line.width(8f).color(color).geodesic(true).addAll(path)
    return this.addPolyline(line)
}

fun GoogleMap.markerAt(position: LatLng, context: Context, drawableRes: Int = R.drawable.oval_blue): Marker {
    return addMarker(MarkerOptions()
            .position(position)
            .draggable(false)
            .icon(bitmapDescriptorFromVector(context, drawableRes)))
}

fun GoogleMap.distance(from: LatLng, to: LatLng ): Double {
    val EARTH_RADIUS = 6371

    val latDistance = Math.toRadians(to.latitude - from.latitude)
    val lonDistance = Math.toRadians(to.longitude - from.longitude)
    val a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2) + (Math.cos(Math.toRadians(from.latitude)) * Math.cos(Math.toRadians(to.latitude))
            * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2))
    val c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a))
    return EARTH_RADIUS.toDouble() * c * 1000.0
}

private fun bitmapDescriptorFromVector(context: Context, vectorResId: Int): BitmapDescriptor {
    val vectorDrawable = ContextCompat.getDrawable(context, vectorResId)
    vectorDrawable.setBounds(0, 0, vectorDrawable.intrinsicWidth, vectorDrawable.intrinsicHeight)
    val bitmap = Bitmap.createBitmap(vectorDrawable.intrinsicWidth, vectorDrawable.intrinsicHeight, Bitmap.Config.ARGB_8888)
    val canvas = Canvas(bitmap)
    vectorDrawable.draw(canvas)
    return BitmapDescriptorFactory.fromBitmap(bitmap)
}



