package org.super8.leto.util

import android.annotation.SuppressLint
import android.app.Application
import android.content.Context
import android.util.Log
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import org.super8.leto.BuildConfig
import org.super8.leto.model.CompanyModel
import org.super8.leto.model.OfferModel
import java.lang.reflect.Type
import javax.inject.Inject

@SuppressLint("StaticFieldLeak")
object SharedPreference {


    private val rootPreferences by lazy { baseContext?.getSharedPreferences("app_root_preferences", Context.MODE_PRIVATE) }
    private val sharedPreferences by lazy { baseContext?.getSharedPreferences(BuildConfig.APPLICATION_ID, Context.MODE_PRIVATE) }
    private var baseContext: Application? = null
    private val gson = Gson()

    @Inject
    fun init(context: Application, version: Long) {
        SharedPreference.baseContext = context
        checkMigration(version)
    }

    private fun checkMigration(initVersion: Long) {
        val currentStorageVersion = rootPreferences?.getLong(ROOT_PREF_PREFERENCES_STORAGE_VERSION, -1)
        Log.i(javaClass.simpleName, "currentStorageVersion: $currentStorageVersion initVersion: $initVersion")
        if (currentStorageVersion != initVersion) {
            clearAll()
            rootPreferences?.edit()?.putLong(ROOT_PREF_PREFERENCES_STORAGE_VERSION, initVersion)?.apply()
        }
    }

    fun save(key: String?, value: String?) {
        sharedPreferences?.edit()?.putString(key, value)?.apply()
    }

    private fun <T> save(key: String?, value: T?) {
        val json = gson.toJson(value)
        sharedPreferences?.edit()?.putString(key, json)?.apply()
    }

    private fun saveSet(key: String?, value: Set<String>) {
        sharedPreferences?.edit()?.putStringSet(key, value)?.apply()
    }

    fun read(key: String, default: String? = null) = sharedPreferences?.getString(key, default)

    private fun <T> read(key: String, default: T? = null, type: Type): T? {
        val jsonValue = sharedPreferences?.getString(key, null) ?: return default
        Log.d(javaClass.simpleName, "read:$key jsonValue = $jsonValue")
        return gson.fromJson(jsonValue, type)
    }

    private fun readSet(key: String) = sharedPreferences?.getStringSet(key, emptySet())

    fun clear(key: String) = sharedPreferences?.edit()?.remove(key)?.apply()

    fun clearAll() {
        sharedPreferences?.edit()?.clear()?.apply()
    }

    private const val ROOT_PREF_PREFERENCES_STORAGE_VERSION = "pref_storage_version"
    private const val PREF_KEY_USER_OFFERS = "user_offers"
    private const val PREF_KEY_USER_PROGRESS = "user_progress"


    var userOffers: ArrayList<OfferModel>?
        set(value) = save(PREF_KEY_USER_OFFERS, value)
        get() = read(PREF_KEY_USER_OFFERS, arrayListOf(), type = object : TypeToken<ArrayList<OfferModel>>() {}.type)

    var userProgress: HashMap<Int, CompanyModel>?
        set(value) = save(PREF_KEY_USER_PROGRESS, value)
        get() = read(PREF_KEY_USER_PROGRESS, hashMapOf(), type = object : TypeToken<HashMap<Int, CompanyModel>>() {}.type)

}