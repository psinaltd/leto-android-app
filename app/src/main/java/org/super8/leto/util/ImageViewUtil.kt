package org.super8.leto.util

import android.widget.ImageView
import com.squareup.picasso.Callback
import com.squareup.picasso.MemoryPolicy
import com.squareup.picasso.NetworkPolicy
import com.squareup.picasso.Picasso


fun ImageView.loadImage(url: String?, callback: (() -> Unit)? = null) {
    if (url == null)
        return
    Picasso.with(context)
            .load(url)
            .fit()
            .centerCrop()
            .networkPolicy(NetworkPolicy.OFFLINE)
            .into(this, object : Callback {
                override fun onSuccess() {
                    callback?.invoke()
                }

                override fun onError() {
                    Picasso.with(context)
                            .load(url)
                            .fit()
                            .centerCrop()
                            .memoryPolicy(MemoryPolicy.NO_CACHE)
                            .networkPolicy(NetworkPolicy.NO_CACHE)
                            .into(this@loadImage, object : Callback {
                                override fun onSuccess() {
                                    callback?.invoke()
                                }

                                override fun onError() {
                                    callback?.invoke()
                                }
                            })
                }
            })
}