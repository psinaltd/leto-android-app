package org.super8.leto

import android.app.Application
import org.super8.leto.dagger.AppComponent
import org.super8.leto.dagger.DaggerAppComponent
import org.super8.leto.util.SharedPreference

class App : Application() {

    companion object {
        lateinit var appComponent: AppComponent
    }

    override fun onCreate() {
        super.onCreate()
        SharedPreference.init(this, 3)
        appComponent = DaggerAppComponent.builder().build()

    }
}