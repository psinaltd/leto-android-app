package org.super8.leto.network

import org.super8.leto.model.MapsResponse
import retrofit2.http.GET
import retrofit2.http.Query
import rx.Observable

interface MapsApi {

    @GET("/maps/api/place/textsearch/json?language=ru&key=AIzaSyAaQHtHgrvy-sDdHLFASYHVRKaHkpb_ews")
    fun getLocationByAddress(@Query("query") address: String): Observable<MapsResponse>
}