package org.super8.leto.network

import android.util.Log
import org.super8.leto.R
import retrofit2.adapter.rxjava.HttpException
import rx.Observable
import rx.Subscription
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers


private val networkErrors = mapOf(
        "Failed to connect to" to R.string.serverConnectionError
//        "timeout" to R.string.serverConnectionError,
//        "timed out" to R.string.serverConnectionError,
//        "Invalid username" to R.string.invalidUserData,
//        "409" to R.string.usernameExist,
//        "Not Found" to R.string.not_found,
//        "User not found" to R.string.user_not_found,
//        "is invalid phone" to R.string.wrong_email,
//        "Unable to resolve host" to R.string.no_network_connection,
//        "was not set" to R.string.was_not_set,
//        "confirmation code is invalid" to R.string.wrong_sms_code,
//        "Internal Server Error" to R.string.internal_server_error,
//        "request access to self ecgs doesn't have a sense" to R.string.self_observe_error,
//        "User account is disabled." to R.string.access_dined,
//        "error on registration" to R.string.server_registration_error,
//        "relation already exists" to R.string.already_add
)

fun <T> simpleNetworkRequest(observable: Observable<T>, onComplete: (T) -> Unit, onError: (Int) -> Unit, rxBlock: (Observable<T>.() -> Observable<T>)? = null): Subscription =
        observable
                .subscribeOn(Schedulers.io())
                .unsubscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .apply {
                    if (rxBlock != null)
                        rxBlock()
                }
                .subscribe(onComplete, {
                    val exception = it
                    exception.printStackTrace()

                    var errorMessageId = R.string.unknownError

                    if (exception is HttpException) {
                        val message = exception.response().errorBody().string()
                        Log.i("NETWORK", "simpleNetworkRequest: error message = $message")
                        for ((key, value) in networkErrors) {
                            if (message.contains(key)) {
                                errorMessageId = value
                                break
                            }
                        }
                    }
                    onError.invoke(errorMessageId)
                })