package org.super8.leto.network

import android.util.Log
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import java.io.File

fun getRequestBody(file: File): MultipartBody.Part {
    val requestBody = RequestBody.create(MediaType.parse("audio/wav"), file)
    return MultipartBody.Part.createFormData("data", file.name, requestBody)
}

fun <T> Call<T>.execute(onResponse: (retrofit2.Response<T>?) -> Unit, onFailure: (Throwable?) -> Unit) {
    try {
        val response = this.execute()
        Log.i(this::class.java.simpleName, "execute: response.isSuccessful = ${response.isSuccessful} response = $response")
        if (response.isSuccessful)
            onResponse.invoke(response)
        else
            onFailure.invoke(Throwable(response.message() + " " + response.errorBody()))

    } catch (e: Exception) {
//        onFailure.invoke(e)
    }
}