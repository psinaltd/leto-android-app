package org.super8.leto.network

import org.super8.leto.model.BaseResponse
import org.super8.leto.model.Trips
import org.super8.leto.model.UserModel
import retrofit2.http.GET
import rx.Observable

interface AitaApi {

    @GET("/api/v1/me/trips?limit=20")
    fun getUserTrips(): Observable<BaseResponse<Trips>>

    @GET("/api/v1/me")
    fun getUserProfile(): Observable<BaseResponse<UserModel>>
}