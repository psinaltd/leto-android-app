package org.super8.leto.dagger

import dagger.Component
import org.super8.leto.ui.activity.MainActivity
import org.super8.leto.dagger.modules.ApiModule
import org.super8.leto.network.AitaApi
import org.super8.leto.network.MapsApi
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(ApiModule::class))
interface AppComponent {

    fun provideAitaApi() : AitaApi
    fun provideMapsApi() : MapsApi

    fun inject(target: MainActivity)
}