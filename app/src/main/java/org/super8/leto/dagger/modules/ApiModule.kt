package org.super8.leto.dagger.modules

import dagger.Module
import dagger.Provides
import org.super8.leto.network.AitaApi
import org.super8.leto.network.MapsApi
import retrofit2.Retrofit
import javax.inject.Named
import javax.inject.Singleton


@Module(includes = arrayOf(RetrofitModule::class))
class ApiModule {

    @Provides
    @Singleton
    fun provideMapsApiModule(@Named("mapsApi") retrofit: Retrofit): MapsApi = retrofit.create(MapsApi::class.java)

    @Provides
    @Singleton
    fun provideAitaApiModule(@Named("aitaApi") retrofit: Retrofit): AitaApi = retrofit.create(AitaApi::class.java)
}