package org.super8.leto.dagger.modules

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Named
import javax.inject.Singleton

@Module
class RetrofitModule {

    companion object {
        //2017-07-07T13:54:13+0000
        val NETWORK_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ssZ"
    }

    @Provides
    @Singleton
    fun superGson(): Gson = GsonBuilder()
//            .setDateFormat(NETWORK_DATE_FORMAT)
//            .serializeNulls()
            .create()

    @Provides
    @Singleton
    fun provideClient(): OkHttpClient = OkHttpClient.Builder()
            .addNetworkInterceptor(HttpLoggingInterceptor().apply { level = HttpLoggingInterceptor.Level.BODY })
            .addInterceptor {
                val request = it.request().newBuilder().addHeader("Authorization", "Bearer waHaaXvoN8dJod7mTNHL4mUhSmv)WcgRwRoWbtmJXrZMNpDwS6v*d3KppT.rEQkUkOaZnraRTXojdAjnMCCu").build()
                it.proceed(request)
            }
            .readTimeout(1, TimeUnit.MINUTES)
            .writeTimeout(1, TimeUnit.MINUTES)
            .connectTimeout(1, TimeUnit.MINUTES)
            .build()

    @Provides
    @Singleton
    fun provideCallAdapterFactory(): RxJavaCallAdapterFactory = RxJavaCallAdapterFactory.create()

    @Provides
    @Singleton
    @Named("aitaApi")
    fun provideAitaRetrofit(gson: Gson, client: OkHttpClient, callAdapterFactory: RxJavaCallAdapterFactory?): Retrofit =
            Retrofit.Builder()
                    .baseUrl("https://iappintheair.appspot.com/")
                    .addCallAdapterFactory(callAdapterFactory)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .client(client)
                    .build()

    @Provides
    @Singleton
    @Named("mapsApi")
    fun provideMapsRetrofit(gson: Gson, client: OkHttpClient, callAdapterFactory: RxJavaCallAdapterFactory?): Retrofit =
            Retrofit.Builder()
                    .baseUrl("https://maps.googleapis.com/")
                    .addCallAdapterFactory(callAdapterFactory)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .client(client)
                    .build()

}